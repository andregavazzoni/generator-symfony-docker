# generator-symfony-docker [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Symfony + Docker + Nginx + PHP7

## Installation

First, install [Yeoman](http://yeoman.io) and generator-symfony-docker using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-symfony-docker
```

Then generate your new project:

```bash
yo symfony-docker
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

Apache-2.0 © [André Gavazzoni]()


[npm-image]: https://badge.fury.io/js/generator-symfony-docker.svg
[npm-url]: https://npmjs.org/package/generator-symfony-docker
[travis-image]: https://travis-ci.org/andregavazzoni/generator-symfony-docker.svg?branch=master
[travis-url]: https://travis-ci.org/andregavazzoni/generator-symfony-docker
[daviddm-image]: https://david-dm.org/andregavazzoni/generator-symfony-docker.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/andregavazzoni/generator-symfony-docker
[coveralls-image]: https://coveralls.io/repos/andregavazzoni/generator-symfony-docker/badge.svg
[coveralls-url]: https://coveralls.io/r/andregavazzoni/generator-symfony-docker
